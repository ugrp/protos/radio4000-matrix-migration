# radio4000 to matrix (migration)

Migrate the data of a [radio4000](https://radio4000.com) channel into
a [matrix](https://matrix.org) room. It will make each r4 track a
matrix event into the room.

> This is not a "matrix bridge". This project sequencially import all
> tracks as new events, without respecting the original published date
> (as we cannot retro-date a new event at an earlier date that it was
> sent).

Currently the "tracks" are created as matrix events of type
`org.libli.room.message.track` when sent into the room (following the
[matrix namespacing naming
convention](https://spec.matrix.org/latest/#namespacing)).

# Conditions of usage & disclaimers
Currently there are many known limitations into "r4 in the matrix ecosystem":
- Use at your own risks, without warranty
- only [libli](https://libli.org) and
  [mwc](https://gitlab.com/sctlib/mwc) support playing the newly
  created matrix event type
- there are only limited features from the original r4 (and latest
  version) available in this "new r4-matrix ecosytem; mainly CRUD
  "track events" and play them in a media player (for now; tbd)
- depends on the https://github.com/internet4000/radio4000-player
- requires to use a libli widget of type `media`, added as "custom
  widget" into the room (copy from libli.com/widgets and add from an
  other client, such as "element web").
- there is a rate limit on matrix homeserver to create

# Usage

Requirements:
1. An exsting radio4000 radio channel with media tracks inside
1. An existing [matrix account](https://matrix.org/try-matrix/)
1. The will to "copy" the data

Get stated:
1. Open [the migration
   web-page](https://ugrp.gitlab.io/protos/radio4000-matrix-migration)
   (this project)
1. Download a radio4000 channel backup file (from our channel; leave
   other people channel out)
1. Select the (r4 channel) backup file to import
1. Select the matrix room to import the tracks into from the backup file
1. Wait until each track is imported

# Notes
Some thing that might be usefull.

## Delete recently sent matrix events (from element web)
In https://app.element.io, when looking at the room we just uploaded a
backup to, we can delete the recently sent events.

In the element GUI right side panel, select the user member of the
room that sent the event, and in "Admin Tools", select "Remove recent
messages" (for this user). We should let "Preserve system messages"
checked, otherwise "matrix room state events" will also be removed
(the events that set the configuration of the room, such as for its
alias, avatar, description, join rules etc.; Unchecking it will
probably "break" the room, until we set-it-up again).

> Use this feature if the import failed to import some track and we
> want to re-import, of if we imported into the wrong room etc.

## Development

In a `node.js` environment we can use `npm run dev` to run a local
development server. Look into the `index.html` file and `src/*` folder
for the code.
