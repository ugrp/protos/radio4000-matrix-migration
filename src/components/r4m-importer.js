import mwc from "../libs/mwc.js";

const formatTime = (timeInMs) => {
	var date = new Date(timeInMs);
	return {
		d: date.getUTCDate() - 1,
		h: date.getUTCHours(),
		m: date.getUTCMinutes(),
		s: date.getUTCSeconds(),
		ms: date.getUTCMilliseconds(),
	};
};

export default class R4MImporter extends HTMLElement {
	EVENT_TYPE_TRACK = "org.libli.room.message.track";
	LIST_NAME = "mwc-joined-rooms";
	delayBetweenRequests = 1871;

	/* set to true for fake api calls */
	get debug() {
		/* return true */
		return this.getAttribute("flag-debug") === "true";
	}

	/* dom helpers */
	get $importPreview() {
		return this.querySelector("r4m-importer-preview");
	}
	get $matrixSearch() {
		return this.querySelector("matrix-search");
	}

	/* api helpers */
	get contextLimit() {
		const max = 200;
		const limit = Number(this.getAttribute("context-limit")) || max;
		return limit > max ? max : limit;
	}
	get filter() {
		return (
			JSON.parse(this.getAttribute("filter")) || {
				types: ["m.room.message", "org.libli.room.message.track"],
				not_types: [
					"m.room.redaction",
					"m.room.member",
					"libli.widget",
					"im.vector.modular.widgets",
					"im.vector.modular.widgets.libli.widget",
					"m.reaction",
					"m.room.history_visibility",
					"m.room.power_levels",
					"m.room.name",
					"m.room.topic",
					"m.space.child",
					"m.room.avatar",
				],
			}
		);
	}

	/* state helpers */
	get timeEstimate() {
		const trackLength = this.channelBackup?.tracks?.length;
		const estimate = this.delayBetweenRequests * trackLength;
		const delta = (estimate * 10) / 100;
		return formatTime(estimate + delta);
	}

	get lastMigratedTrack() {
		const messages = this.messages.chunk;
		if (messages?.length) {
			const lastEventImported = messages.find(event => event?.content?.r4_id);
			if (lastEventImported) {
				return this.messagesMap.get(lastEventImported?.content?.r4_id);
			}
		}
	}

	/* events */
	async onFileInput({ target: { files } }) {
		if (files.length) {
			const file = files[0];
			const text = await file.text();
			const data = JSON.parse(text);
			if (data.channel && data.tracks) {
				this.channelBackup = data;
				this.renderImportPreview();
			} else if (data.messages && data.export_date) {
				window.alert(
					"It seems that the file is a matrix room export, not a r4 backup",
				);
			}
		}
	}
	async onSearchInput(event) {
		await this.handleRoomSelect(event.target.value);
	}
	async onImportSubmit(event) {
		const $button = event.target;
		$button.setAttribute("disabled", true);

		event.preventDefault();
		if (this.channelBackup && this.room) {
			const message = `Confirm import radio4000 channel: ${this.channelBackup.channel.slug} into matrix room: ${this.room.state.alias}. This might take a while, don't close the window until finished.`;
			if (window.confirm(message)) {
				// also doing the "status render"
				this.trackImportStatuses = await this.importTracks();
				this.renderDoneImportStatus();
			}
		}
		$button && $button.removeAttribute("disabled");
	}
	onSearchSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		const alias = new FormData(event.target).get("search_term");
		this.handleRoomSelect(alias);
	}

	async handleRoomSelect(alias) {
		if (!alias) {
			return;
		}
		const profile = mwc.api.checkMatrixId(alias);
		const { profileId, roomAlias } = profile;
		if (roomAlias) {
			this.$matrixSearch.disableForm();
			this.room = await mwc.api.getRoomInfo(profile);
			if (this.room?.state) {
				this.messages = await mwc.api.getRoomMessages({
					roomId: this.room.state.id,
					params: [
						["filter", this.filter],
						["dir", "b"],
						["limit", this.contextLimit],
					],
				});
				this.messagesMap = this.messagesToTrackMap(this.messages);
			}
		} else {
			this.room = null;
		}
		this.$matrixSearch.enableForm();
		this.renderImportPreview();
	}

	/* methods */
	async importTracks() {
		let importResponses = [];
		const sortedTracks =
			this.channelBackup?.tracks?.sort((a, b) => {
				return new Date(a.created) - new Date(b.created);
			}) || [];


		let notYetImportedTracks;
		if (!this.lastMigratedTrack?.content?.r4_id) {
			notYetImportedTracks = sortedTracks;
		} else {
			notYetImportedTracks = sortedTracks.filter((track) => {
				const trackCreatedDate = new Date(track?.created);
				const lastEventR4CreatedDate = new Date(
					this.lastMigratedTrack?.content?.r4_created,
				);
				const shouldNotBeUploaded = trackCreatedDate > lastEventR4CreatedDate;
				return shouldNotBeUploaded;
			})
		}

		const totalLen = this.channelBackup?.tracks?.length;
		const alreadyImportedLen = totalLen - notYetImportedTracks.length;

		for (const r4Track of notYetImportedTracks) {
			const count = alreadyImportedLen + importResponses.length;
			const count_left = totalLen - count;
			const importStatus = {
				total_len: totalLen,
				already_imported_len: alreadyImportedLen,
				to_import_len: notYetImportedTracks.length,
				count_left,
				count,
			};

			const existingTrackEvent = this.messagesMap.get(r4Track.id);
			if (existingTrackEvent) {
				importStatus.r4Track = r4Track;
				importStatus.matrixEvent = existingTrackEvent;
				console.info("Skipping already imported track", importStatus);
				importResponses.push(importStatus);
				this.renderImportStatus(importStatus);
			} else {
				await new Promise((resolve) =>
					setTimeout(resolve, this.delayBetweenRequests),
				);
				try {
					const importRes = await this.importTrack(r4Track);
					const { retries, eventRes, matrixEvent } = importRes;
					importStatus.retries = retries;
					importStatus.eventRes = eventRes;
					importStatus.r4Track = r4Track;
					importStatus.matrixEvent = matrixEvent;
					importResponses.push(importStatus);
					console.info("track/event importStatus", importStatus);
					this.renderImportStatus(importStatus);
				} catch (error) {
					console.error("Error importing track:", error);
					// Handle error or retry logic
				}
			}
		}
		return importResponses;
	}
	async importTrack(r4Track, retries = 0) {
		const maxRetries = 5;
		const content = {
			body: r4Track.body,
			title: r4Track.title,
			url: r4Track.url,
			discogs_url: r4Track.discogsUrl,
			r4_id: r4Track.id,
			r4_created: r4Track.created,
		};

		const matrixEvent = {
			room_id: this.room.state.id,
			event_type: this.EVENT_TYPE_TRACK,
			content,
		};
		let eventRes;
		try {
			if (this.debug) {
				eventRes = await new Promise((resolve) =>
					setTimeout(
						() => resolve({ event_id: "DEBUG_MOCK" }),
						this.delayBetweenRequests,
					),
				);
			} else {
				/* delay not to be rate limited by the homeserver API */
				await new Promise((resolve) =>
					setTimeout(resolve, this.delayBetweenRequests),
				);
				eventRes = await mwc.api.sendEvent(matrixEvent);
			}

			/* if we are ratelimite, wait and retry */
			if (eventRes?.errcode === "M_LIMIT_EXCEEDED" && retries < maxRetries) {
				console.log("Import failed; adding delay before retrying", r4Track);
				await new Promise((resolve) =>
					setTimeout(
						resolve,
						this.delayBetweenRequests + eventRes.retry_after_ms * 4,
					),
				);
				console.log("Retrying import for", r4Track);
				return await this.importTrack(r4Track, retries + 1);
			}
		} catch (importError) {
			console.info("Error importing", importError);
			eventRes = importError;
		}
		return {
			eventRes,
			matrixEvent,
			retries,
		};
	}
	messagesToTrackMap(mxMessages) {
		const { chunk } = mxMessages;
		const trackMap = new Map();
		chunk?.forEach((mxEvent) => {
			trackMap.set(mxEvent.content.r4_id, mxEvent);
		});
		return trackMap;
	}

	async connectedCallback() {
		this.render();
	}
	render() {
		const $file = this.createReadFile();
		const $roomSelect = this.createRoomSelect();
		this.replaceChildren($file, $roomSelect);
	}
	renderImportPreview() {
		const $roomPreview = this.createImportPreview();
		if (this.$importPreview) {
			this.$importPreview.replaceWith($roomPreview);
		} else {
			this.append($roomPreview);
		}
	}
	renderImportStatus(status) {
		const { total_len, count, matrixEvent, r4Track } = status;
		const $importing = document.createElement("r4m-importer-status");

		const $progress = document.createElement("progress");
		$progress.setAttribute("value", count);
		$progress.setAttribute("max", total_len);
		const $progressMessage = document.createElement("pre");
		$progressMessage.textContent = JSON.stringify(status, null, 2);
		$importing.append($progress, $progressMessage);
		this.replaceChildren($importing);
	}
	renderDoneImportStatus() {
		if (this.trackImportStatuses) {
			const $doneStatus = this.createTrackImportStatus();
			this.replaceChildren($doneStatus);
		}
	}
	createImportPreview() {
		const $importPreview = document.createElement("r4m-importer-preview");
		if (this.room) {
			const $roomPreview = this.createRoomPreview();
			$importPreview.append($roomPreview);
		} else {
			const $noRoom = document.createElement("code");
			$noRoom.textContent = "missing matrix room alias";
			$importPreview.append($noRoom);
		}
		if (this.channelBackup) {
			const $channelPreview = this.createChannelPreview();
			$importPreview.append($channelPreview);
		} else {
			const $noChannelBackup = document.createElement("p");
			$noChannelBackup.textContent = "missing radio4000 backup file";
			$importPreview.append($noChannelBackup);
		}

		const $importData = document.createElement("form");
		$importData.addEventListener("submit", this.onImportSubmit.bind(this));
		const $importDataSubmit = document.createElement("button");
		$importDataSubmit.setAttribute("type", "submit");
		$importDataSubmit.textContent = "Import tracks to matrix room";
		if (this.channelBackup && this.room) {
		} else {
			$importDataSubmit.setAttribute("disabled", true);
		}
		$importData.append($importDataSubmit);
		$importPreview.append($importData);
		return $importPreview;
	}
	createReadFile() {
		const $form = document.createElement("form");
		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.textContent = "Read a radio4000 channel backup file";
		const $input = document.createElement("input");
		$input.addEventListener("input", this.onFileInput.bind(this));
		$input.setAttribute("type", "file");
		$input.setAttribute("accept", "file/json");
		$input.setAttribute("required", true);
		$fieldset.append($legend, $input);
		$form.append($fieldset);
		return $form;
	}
	createRoomSelect() {
		const $matrixSearch = document.createElement("matrix-search");
		$matrixSearch.addEventListener("input", this.onSearchInput.bind(this));
		$matrixSearch.addEventListener("submit", this.onSearchSubmit.bind(this));
		$matrixSearch.setAttribute("quicksearch", true);
		$matrixSearch.setAttribute(
			"legend",
			"Select matrix room alias to import into",
		);
		$matrixSearch.setAttribute("placeholder", "Room alias");
		$matrixSearch.textContent = "… loading joined matrix rooms";
		return $matrixSearch;
	}
	createRoomPreview() {
		const $roomState = document.createElement("matrix-room-state");
		$roomState.setAttribute("state", JSON.stringify(this.room.state));
		return $roomState;
	}
	createChannelPreview() {
		const t = this.timeEstimate;
		const time = `${t.d}d-${t.h}h-${t.m}m- ${t.s}s`;
		const $channel = document.createElement("r4m-importer-channel");
		$channel.textContent = `${this.channelBackup.tracks.length} tracks to import (approx time: ${time})`;
		return $channel;
	}
	createTrackImportStatus() {
		const $wrapper = document.createElement("r4m-importer-finished");
		const $imports = this.trackImportStatuses.map((trackStatus) => {
			const { eventRes } = trackStatus;
			const { event_id } = eventRes || {};
			const $trackEventStatus = document.createElement("r4m-importer-event");
			$trackEventStatus.setAttribute("done", !!event_id);

			if (event_id) {
				const $trackEventStatusLink = this.createStatusLink(trackStatus);
				$trackEventStatus.append($trackEventStatusLink);
			} else {
				const $trackEventStatusError = this.createTrackImportError(trackStatus);
				$trackEventStatus.append($trackEventStatusError);
			}
			return $trackEventStatus;
		});

		const $finishedMessage = document.createElement("p");
		$finishedMessage.textContent = "Import finished";
		$wrapper.append($finishedMessage, ...$imports);
		return $wrapper;
	}
	createStatusLink(trackStatus) {
		const href = `https://libli.org/${this.room.state.alias}/${trackStatus.eventRes.event_id}`;
		const $trackEventStatusLink = document.createElement("a");
		$trackEventStatusLink.setAttribute("href", href);
		$trackEventStatusLink.setAttribute("target", "_blank");
		$trackEventStatusLink.setAttribute("rel", "noref noopener");
		$trackEventStatusLink.textContent = trackStatus.r4Track.title;
		return $trackEventStatusLink;
	}
	createTrackImportError(trackStatus) {
		const $trackEventStatusError = document.createElement(
			"r4m-importer-import-error",
		);
		const $trackEventStatusRetry = document.createElement("button");
		$trackEventStatusRetry.addEventListener("click", async (event) => {
			event.target.setAttribute("disabled", true);
			try {
				const retryRes = await this.uploadTrack(trackStatus.r4Track);
				if (!retryRes.error) {
					event.target.remove();
				}
			} catch (retryError) {
				consol.log("Error retrying track import", retryError);
			}
			event.target.setAttribute("disabled", true);
		});
		$trackEventStatusError.append($trackEventStatusRetry);
		return $trackEventStatusError;
	}
}
