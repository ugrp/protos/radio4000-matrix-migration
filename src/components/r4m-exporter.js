import mwc from "../libs/mwc.js";

const SUPABASE_URL = "https://oupgudlkspsmzkmeovlh.supabase.co"
const SUPABASE_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im91cGd1ZGxrc3BzbXprbWVvdmxoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM0NTIwNzQsImV4cCI6MjAyOTAyODA3NH0.KAbKFBChJHtxTmOZM2pdeppIyNbcnQkEgSi6RA7OQdo"

const createBackupV2 = (({channel = {}, tracks = []} = {}) => {
	const sChannel = {
		id: channel.id,
		supabase_id: channel.id,
		firebase_id: channel.firebase_id,
		slug: channel.slug,
		name: channel.name,
		body: channel.description,
		created: channel.created_at,
		updated: channel.updated_at,
		link: channel.url,
		image: channel.image,
		imageUrl: `${!channel.image ? channel.image : "https://res.cloudinary.com/radio4000/" + channel.image}`,
		coordinatesLongitude: channel.longitude,
		coordinatesLatitude: channel.latitude,
	}
	const sTracks = tracks?.map(track => {
		return {
			id: track.id,
			url: track.url,
			title: track.title,
			body: track.description,
			updated: track.updated_at,
			created: track.created_at,
			discogsUrl: track.discogs_url
		}
	})
	return {
		backup_version: "v2.r4m",
		channel: sChannel,
		tracks: sTracks,
		favorites: [],
		followers: [],
	}
})

const SDKS = {
	v2: {
		fetchAllChannels: async () => {
			return fetch(`${SUPABASE_URL}/rest/v1/channels?apikey=${SUPABASE_KEY}&select=*&order=slug.asc&offset=0&limit=10000&`).then(async (res) => {
				const data = await res.json()
				if (res.status === 200) {
					return data
				} else {
					throw data
				}
			})
		},
		getChannelBackupUrl: (slug) => {
			return `${SUPABASE_URL}/rest/v1/backup_channels?apikey=${SUPABASE_KEY}&select=*&slug=eq.${slug}`
		},
		getChannelUrl: (slug) => {
			return `${SUPABASE_URL}/rest/v1/channels?apikey=${SUPABASE_KEY}&select=*&slug=eq.${slug}`
		},
		getChannelTracksUrl: (slug) => {
			return `${SUPABASE_URL}/rest/v1/channel_tracks?apikey=${SUPABASE_KEY}&select=*&slug=eq.${slug}&order=created_at.desc&limit=4000&offset=0`
		},
		downloadBackup: async (slug) => {
			let channel;
			try {
				channel = await fetch(SDKS.v2.getChannelUrl(slug)).then(async (res) => {
					const data = await res.json()
					if (res.status === 200) {
						const channel = data[0]
						console.log("channel", channel)
						return channel
					} else {
						throw data
					}
				})
			} catch(channelError) {
				throw channelError
			}
			let tracks;
			if (channel) {
				try {
					tracks = await fetch(SDKS.v2.getChannelTracksUrl(slug)).then(async (res) => {
						const data = await res.json()
						return data
					})
				} catch(channelTracksError) {
					throw channelTracksError
				}
			}
			return createBackupV2({
				channel,
				tracks,
			})
		}
	},
	v1: {
		fetchAllChannels: async () => {
			return fetch("https://radio4000.firebaseio.com/channels.json").then(
				async (res) => {
					const channels = await res.json();
					const entries = Object.entries(channels).reduce(
						(acc, [channelId, channel]) => {
							const newChannel = { ...channel, id: channelId };
							acc.set(channel.slug, newChannel);
							return acc;
						},
						new Map(),
					);
					return entries;
				},
			);
		},
		getChannelBackupUrl: (slug) => {
			return `https://api.radio4000.com/backup?slug=${slug}`
		}
	}
}
const VERSIONS = Object.keys(SDKS)

export default class R4MExporter extends HTMLElement {

	/* methods */
	fetchAllChannels() {
		return this.sdk.fetchAllChannels()
	}
	getChannelBackupUrl(slug) {
		/* CORS prevent Us to fetch the URL … */
		return this.sdk.getChannelBackupUrl(slug);
	}

	/* dom refs */
	LIST_NAME = "r4-channels";

	/* events */
	async onVersionSelect(event) {
		this.version = event.target.value
		this.sdk = SDKS[this.version]
		this.channels = await this.fetchAllChannels();
		this.render()
	}
	async onChannelSelect(event) {
		this.slug = event.target.value
	}
	async onCreateBackup(event) {
		event.preventDefault();
		if (this.slug) {
			if (this.version === "v1") {
				this.backupUrl = this.getChannelBackupUrl(this.slug);
			} else if (this.version === "v2") {
				try {
					const backup = await this.sdk.downloadBackup(this.slug)
					const backupEncoded = encodeURIComponent(JSON.stringify(backup))
					this.backupUrl = `data:text/json;charset=utf-8,${backupEncoded}`
				} catch(backupError) {
					throw backupError
				}
			}
		}
		this.render()
	}
	async connectedCallback() {
		this.version = VERSIONS[0]
		this.sdk = SDKS[this.version]
		this.channels = await this.fetchAllChannels();
		this.slug = null
		this.backupUrl = null
		this.render();
	}
	render() {
		let $doms = []
		if (this.backupUrl) {
			$doms.push(
				this. createDownloadLink({version: this.version, slug: this.slug, url: this.backupUrl})
			)
		} else {
			$doms.push(
				this.createVersionSelect(this.version),
				this.createChannelSelect(this.slug, this.channels),
				this.createButton(),
			)
		}
		const $form = this.createForm()
		$form.replaceChildren(...$doms.filter($dom => !!$dom));
		this.replaceChildren($form);
	}
	createVersionSelect(currentVersion) {
		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.textContent = "Select radio4000 version (1 or 2)";
		const $select = document.createElement("select");
		$select.addEventListener("input", this.onVersionSelect.bind(this))
		$select.setAttribute("name", "version");
		$select.setAttribute("list", this.LIST_NAME);
		const $options = VERSIONS.map(version => {
			const $option = document.createElement("option");
			$option.setAttribute("value", version);
			$option.textContent = version
			if (currentVersion === version) {
				$option.selected = true
			}
			return $option
		});
		$select.append(...$options)
		$fieldset.append($legend, $select);
		return $fieldset
	}
	createChannelSelect(slug, channels = []) {
		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.textContent = "Select radio4000 channel to backup";
		const $input = document.createElement("input");
		if (slug) {
			$input.value = slug
		}
		$input.setAttribute("name", "slug");
		$input.setAttribute("list", this.LIST_NAME);
		$input.addEventListener("input", this.onChannelSelect.bind(this))
		const $datalist = document.createElement("datalist");
		$datalist.setAttribute("id", this.LIST_NAME);
		const $options = channels?.forEach(({ slug }) => {
			const $option = document.createElement("option");
			$option.setAttribute("value", slug);
			$datalist.append($option);
		});
		$fieldset.append($legend, $input, $datalist);
		return $fieldset
	}
	createButton() {
		const $btnFieldset = document.createElement("fieldset");
		const $btnLegend = document.createElement("legend");
		$btnLegend.textContent = "Create channel backup file";
		const $btn = document.createElement("button");
		$btn.setAttribute("type", "submit");
		$btn.textContent = "Create backup file";
		$btnFieldset.append($btnLegend, $btn);
		return $btnFieldset
	}
	createDownloadLink({version, slug, url}) {
		const filename = "radio4000-backup"
		const ext = "json"
		const date = new Date();
		const dateFormatter = Intl.DateTimeFormat('sv-SE');
		const today = dateFormatter.format(date)
		const download = `${slug}_${filename}_${version}_${today}.${ext}`
		const $anchor = document.createElement("a")
		$anchor.setAttribute("download", download)
		$anchor.setAttribute("href", url)
		$anchor.setAttribute("target", "_blank")
		if (!version || !slug || !url) {
			$anchor.setAttribute("disabled", true)
			$anchor.textContent = "To create a backup link, select a channel";
		} else {
			$anchor.textContent = `Download backup file: ${download}`;
		}
		return $anchor
	}
	createForm() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onCreateBackup.bind(this));
		return $form;
	}
}
