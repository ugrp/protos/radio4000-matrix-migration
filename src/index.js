import R4MImporter from "./components/r4m-importer.js";
import R4MExporter from "./components/r4m-exporter.js";

customElements.define("r4m-importer", R4MImporter);
customElements.define("r4m-exporter", R4MExporter);
